package e.anupam.list_example;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapter extends ArrayAdapter<String>
{
    public CustomAdapter(@NonNull Context context, String[] subs)
    {
        super(context, R.layout.custom_row, subs);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        LayoutInflater myinflator = LayoutInflater.from(getContext());
        View customview = myinflator.inflate(R.layout.custom_row,parent,false);
        String temp = getItem(position);
        ImageView myimage = (ImageView) customview.findViewById(R.id.imageView2);
        TextView mytext = (TextView) customview.findViewById(R.id.text);
        mytext.setText(temp);
        myimage.setImageResource(R.drawable.index);
        return customview;
    }


}
