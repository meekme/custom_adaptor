package e.anupam.list_example;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String[] foods = {"fish","chicken","egg","pork","cheese"};
        ListAdapter adapter = new CustomAdapter(this,foods);
        ListView mylist = (ListView) findViewById(R.id.list);
        mylist.setAdapter(adapter);
        mylist.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String subj = String.valueOf(adapterView.getItemAtPosition(i));
                        Toast.makeText(MainActivity.this,subj,Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}
